﻿#PARAMETRE

#"INSERT INTO t_users (useLastName, useFirstName, useLogin, useAirpods, useGlasses) VALUES('Test', 'Test', 'test', 0, 1)"

param([string]$pathCSV)


Write-Host "Check"
#
#Information pour la connexion à la base de données
$sqlServer = "localhost\SQLPROJET"

$logPath = 

#Nom de la base
$sqlDBName = "p_script"
#Login et pwd
$user = "MICHEL-PC"
$password = ".Etml-"

function ExecuteQueryLoadData($path)
{
    #connexion à la base de données
    $sqlConnection = New-Object System.Data.SqlClient.SqlConnection
    $sqlConnection.ConnectionString = "Server = $sqlServer; Database = $sqlDBName; integrated Security = True; User ID = $user; Password = $password;"

    #Ouvrir la connexion avec la base
    $sqlConnection.Open()

    #requetes
    $sqlQuery = "BULK INSERT t_users
                    FROM '$path'
                    WITH (FIRSTROW = 2,
                    FIELDTERMINATOR = ';',
	                ROWTERMINATOR='\n');"

    $sqlCommand = New-Object System.Data.SQLClient.SQLCommand($sqlQuery, $sqlConnection)
    
    try
    {
        $sqlCommand.ExecuteNonQuery()
        if(!(Test-Path ".\SuccessLog.txt"))
        {
            $date = Get-Date
            New-Item -Path ".\" -ItemType "file" -Name "SuccessLog.txt" -Value "$date : Les données du fichier $path ont été insérées avec succès !`r`n"
        }
        else
        {
            $date = Get-Date
            Add-Content ".\SuccessLog.txt" "$date : Les données du fichier $path ont été insérées avec succès !`r`n"
        }
    }
    catch
    {
        if(!(Test-Path ".\ErrorLog.txt"))
        {
            $date = Get-Date
            New-Item -Path ".\" -ItemType "file" -Name "ErrorLog.txt" -Value "$date : Erreur lors de l'importation du fichier $path`r`n"
        }
        else
        {
            $date = Get-Date
            Add-Content ".\ErrorLog.txt" "$date : Erreur lors de l'importation du fichier $path`r`n"
        }
    }
    
    
    
    $sqlConnection.Close()
}

function ExecuteQueryGetResult($sqlQuery)
{
    #connexion à la base de données
    $sqlConnection = New-Object System.Data.SqlClient.SqlConnection
    $sqlConnection.ConnectionString = "Server = $sqlServer; Database = $sqlDBName; integrated Security = True; User ID = $user; Password = $password;"

    #Ouvrir la connexion avec la base
    $sqlConnection.Open()

    #requetes
    $query = $sqlQuery
    $sqlCommand = New-Object System.Data.SQLClient.SQLCommand($query, $sqlConnection)
    $sqlAdapter = New-Object System.Data.SQLClient.SQLDataAdapter($sqlCommand)
    $dataSet = New-Object System.Data.DataSet
    
    $sqlAdapter.Fill($dataSet)
    
    $sqlConnection.Close()
    
    return $dataSet
    
}

ExecuteQueryLoadData($pathCSV)
write-host "DONE"
